var express = require('express');
var path = require('path');

var router = express.Router();

//temp
var careers = require('../db/careers.json')
//

// db extract to different route or mongo
router.get('/db/careers', (req, res) => {
  res.sendFile(path.join(__dirname, '../db', 'careers.json'));
})
//

/* GET home page. */
router.get('/', (req, res, next) => {
  res.render('index');
});

router.get('/careers/:id', (req, res, next) => {
  const job = careers[req.params.id].en;
  const jobObject = {
    job_title: job.title,
    job_description: job.description,
    job_requirements: job.job_requirements,
    job_technical_requirements: job.technical_job_requirements,
    job_offers: job.offer,
    footer_btn_text: 'Apply',
    footer_btn_email: 'become@kaayman.de'
  };

  res.render('career-template', jobObject);
})

router.get('/careers', (req, res, next) => {
  res.render('careers', {
    footer_btn_text: 'Apply',
    footer_btn_email: 'become@kaayman.de'
  })
})

router.get('/:url', (req, res, next) => {
  res.render(req.params.url);
});

module.exports = router;
