const svgPath = "/images/approach/approach.svg";

const runAnimation = () => {
    var s = Snap("#approach-animation");

    Snap.load(svgPath, (data) => onSVGLoaded(data, s));
}

function onSVGLoaded(data, s) {
    s.append(data);
    registerAnimation(s)
}

function registerAnimation(crocodile) {

    const object = crocodile.select("#approach");
    const wrapper = object.select("#for_web");
    const upperLeft = object.select("#upper_left");
    const upperRight = object.select("#upper_right");
    const lowerRight = object.select("#lower_right");
    const lowerLeft = object.select("#lower_left");


    registerHoverAnimation(upperLeft, () => animateComponent(upperLeft, 1.3), () => animateComponent(upperLeft, 1.0))
    registerHoverAnimation(upperRight, () => animateComponent(upperRight, 1.3), () => animateComponent(upperRight, 1.0))
    registerHoverAnimation(lowerRight, () => animateComponent(lowerRight, 1.3), () => animateComponent(lowerRight, 1.0))
    registerHoverAnimation(lowerLeft, () => animateComponent(lowerLeft, 1.3), () => animateComponent(lowerLeft, 1.0))
}

function registerHoverAnimation(component, hoverInFunction, hoverOutFunction) {
    component.hover(hoverInFunction, hoverOutFunction);
}

function animateComponent(component, scaleValue) {
    component.stop().animate({
        transform: `s${scaleValue}`
    }, 1000)
}

window.onload = runAnimation;